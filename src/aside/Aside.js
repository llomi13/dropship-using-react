import logo from "../aside/pictures/dropship_logo.png";
import { IconContext } from "react-icons";
import { CgProfile } from "react-icons/cg";
import {
  FaCompass,
  FaCube,
  FaCartArrowDown,
  FaClipboardCheck,
  FaClipboardList,
} from "react-icons/fa";
import { RiExchangeLine } from "react-icons/ri";
import { MdList } from "react-icons/md";
const Sidebar = () => {
  return (
    <aside className="aside">
      <div className="aside__nav">
        <div>
          <img src={logo} class="dropship-logo" />
        </div>
        <IconContext.Provider value={{ size: "1.3em" }}>
          <div>
            <CgProfile className="aside__nav-logo" />
          </div>
          <div>
            <FaCompass className="aside__nav-logo" />
          </div>
          <div>
            <MdList className="aside__nav-logo" />
          </div>
          <div>
            <FaCube className="aside__nav-logo" />
          </div>
          <div>
            <FaCartArrowDown className="aside__nav-logo" />
          </div>
          <div>
            <FaClipboardCheck className="aside__nav-logo" />
          </div>
          <div>
            <RiExchangeLine className="aside__nav-logo" />
          </div>
          <div>
            <FaClipboardList className="aside__nav-logo" />
          </div>
        </IconContext.Provider>
      </div>
      <div className="aside__categories">
        <div>
          <select className="choose-niche">
            <option value="choose-niche">Choose Niche</option>
            <option value="...">...</option>
            <option value="best sellers">Best Sellers</option>
            <option value="beauty">Beauty</option>
            <option value="elecronics">Electronics</option>
            <option value="fashion">Fashion</option>
            <option value="fragrances">Fragrances</option>
            <option value="health">Health</option>
            <option value="home-design">Home & Design</option>
            <option value="innovative-outlet">Innovative & outlet</option>
            <option value="jewelry">Jewelry</option>
            <option value="pets">Pets</option>
            <option value="sex-toys">Sex Toys</option>
            <option value="toys">Toys</option>
          </select>
          <select className="choose-category">
            <option value="choose-category">Choose Category</option>
          </select>
        </div>
        <div className="categories-inner">
          <div>
            <select className="ship-from">
              <option value="ship-from">Ship From</option>
            </select>
          </div>
          <div>
            <select className="ship-to">
              <option value="ship-to">Ship To</option>
            </select>
          </div>
          <div>
            <select className="select-supplier">
              <option value="select-supplier">Select Supplier</option>
            </select>
          </div>
          <div class="price">
            <div>
              <span>Price Range</span>
            </div>
            <div>
              <input type="range" class="input-price" />
            </div>
          </div>
          <div class="profit">
            <div>
              <span>Profit Range</span>
            </div>
            <div>
              <input type="range" class="input-profit" />
            </div>
          </div>
          <button class="nav__button nav__button-aside">Reset Filter</button>
        </div>
      </div>
    </aside>
  );
};

export default Sidebar;
