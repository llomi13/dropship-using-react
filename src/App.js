import Header from "./header/Header";
import Catalog from "./catalog/Catalog";
import Sidebar from "./aside/Aside";
import axios from "axios";
import React, { useEffect, useState } from "react";

function App() {
  const [products, setProducts] = useState([]); // product data
  const [search, setSearch] = useState("");
  const [filteredProducts, setFilteredProducts] = useState({});
  const [count, setCount] = useState(0); // number of products selected
  // useEffect(() => {
  //   axios
  //     .get(`https://fakestoreapi.com/products`)
  //     .then((res) => {
  //       console.log(res);
  //       setProducts(res.data);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // }, []);

  useEffect(() => {
    axios
      .get("https://fakestoreapi.com/products")
      .then((res) => {
        localStorage.setItem("products", JSON.stringify(res.data));
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    setProducts(JSON.parse(localStorage.getItem(`products`)));
  }, []);
  useEffect(() => {
    setFilteredProducts(
      products.filter((product) =>
        product.title.toLowerCase().includes(search.toLowerCase())
      )
    );
  }, [search, products]);

  const onChange = (e) => {
    setSearch(e.target.value);
  };

  return (
    <div className="dropship">
      <div>
        <Sidebar />
      </div>
      <div>
        <Header
          onChange={onChange}
          count={count}
          products={products}
          setProducts={setProducts}
        />
        <Catalog
          products={products}
          filteredProducts={filteredProducts}
          setCount={setCount}
        />
      </div>
    </div>
  );
}

export default App;
