import Select from "../header/SelectButton";
import Inventory from "../header/AddInventory";
import Option from "../header/OptionButton";
import SearchOnClick from "../header/SearchQuery";
import SearchBar from "./SearchBar"

const Header = ({onChange, count, products, setProducts, }) => {
  return (
    <header className="header">
      <nav className="nav">
        <div className="nav__select">
          <Select title={"SELECT ALL"}/>
          <h2>selected {count} out of {products.length}</h2>
          {count > 0 ? <Select title={"CLEAR SELECTED"}/> : ""}
        </div>
        <div className="nav__search">
          <SearchBar onChange={onChange}/>
          <SearchOnClick />
          <Inventory />
          <button class="help-menu">
          <span>?</span>
          </button>
        </div>
      </nav>
      <Option products = {products} setProducts = {setProducts}/>
    </header>
  );
};

export default Header;
