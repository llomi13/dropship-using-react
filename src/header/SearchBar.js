const SearchBar = ({onChange}) => {
  return <input type="text" onChange={onChange} placeholder="Search..." id="searchQuery" />;
};

export default SearchBar;
