import { GrSort } from "react-icons/gr";

const Option = ({ products, setProducts }) => {
  const sortCatalog = (e) => {
    const sort = e.target.value;
    const newProducts = [...products];
    if (sort === "asc") {
      const sorted = newProducts.sort((a, b) => b.price - a.price);
      setProducts(sorted);
    } else if (sort === "desc") {
      const sorted = newProducts.sort((a, b) => a.price - b.price);
      setProducts(sorted);
    } else if (sort === "az") {
      const sorted = newProducts.sort((a, b) => a.title.localeCompare(b.title));
      setProducts(sorted);
    } else if (sort === "za") {
      const sorted = newProducts.sort((a, b) => b.title.localeCompare(a.title));
      setProducts(sorted);
    } else if (sort === "unsorted") {
      const sorted = newProducts;
      setProducts(sorted);
    }
  };
  return (
    <nav className="nav nav-sort">
      <div className="nav__sort">
        <select id="sort" onChange={sortCatalog}>
          <option value="unsorted">Sort By: New Arrivals</option>
          <option value="asc">Price High To Low</option>
          <option value="desc">Price Low To High</option>
          <option value="az">A-Z</option>
          <option value="za">Z-A</option>
        </select>
      </div>
    </nav>
  );
};

export default Option;
