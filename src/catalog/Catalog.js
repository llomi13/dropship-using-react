import Products from "./Products";

const Catalog = ({products , filteredProducts,  setCount}) => {
  
  return (
    <section className="catalog">
     <Products products={products} filteredProducts={filteredProducts} setCount = {setCount}/>
    </section>
  );
};

export default Catalog;
