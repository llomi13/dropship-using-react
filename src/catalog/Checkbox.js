import { useState } from "react";

const CheckBox = ({setCount}) => {
  const [checked, setChecked] = useState(false);
  const changeChecked = (e) => {
    setChecked(e.target.checked);
    setCount(prevCount => prevCount + (e.target.checked ? 1 : -1));
  };
  return (
    <label class = "checkbox-container">
       <input
      type="checkbox"
      className="catalog__checkbox"
      value={checked}
      onChange = {changeChecked}
     />
     <span class = "catalog__checkmark"></span>
    </label>
   
  );
};
export default CheckBox;

