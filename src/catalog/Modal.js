import "../catalog/modal.css";
import { AiFillCloseCircle } from "react-icons/ai";

const Modal = ({ product, setCloseModal }) => {
  return (
    <div className="modal">
      <div className="modal__content">
        <div className="modal__left-side">
          <div className="modal__photo">
            <img src={product.image} />
          </div>
          <div className="modal__category">{product.category}</div>
          <div className="modal__price">COST: ${product.price}</div>
        </div>
        <div className="modal__right-side">
          <h3 className="modal__title">{product.title}</h3>
          <h4 className="modal__description-title">Description</h4>
          <hr />
          <p className="modal__description">{product.description}</p>
        </div>
        <div className="close-modal" onClick={setCloseModal}>
          <i>
            {" "}
            <AiFillCloseCircle size="1.3em" />{" "}
          </i>
        </div>
      </div>
    </div>
  );
};

export default Modal;
