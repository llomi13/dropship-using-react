import CheckBox from "./Checkbox";
import { useState } from "react";
import Modal from "./Modal"

const Product = ({  setCount, product }) => {
  const [openModal, setOpenModal] = useState(false);
  const showModal = () => {
    setOpenModal(true);
  };

  const setCloseModal = () => {
    setTimeout(() => setOpenModal(false), 100);
  };
  return (
    <div className="catalog__product">
      <div className="catalog__image" >
        <CheckBox setCount={setCount} />
        <img src={product.image} onClick={showModal} />
      </div>
      <div className="catalog__text">
        <h2>{product.title}</h2>
      </div>
      <div className="catalog__prices">
        <h3>{product.price + "$"}</h3>
      </div>
      {openModal && <Modal product={product} setCloseModal={setCloseModal} />}
    </div>
  );
};

export default Product;
