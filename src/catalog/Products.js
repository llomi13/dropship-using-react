import Product from "./Product";
const Products = (props) => {
  const products = props.filteredProducts.length
    ? props.filteredProducts
    : props.products;
  return (
    <main className="main">
      {products.map((product) => (
        <Product
          setCount={props.setCount}
          key={product.id}
          title={product.title}
          product = {product}
        />
      ))}
    </main>
  );
};

export default Products;
